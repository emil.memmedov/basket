import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { PopUpErrorComponent } from './pop-up-error/pop-up-error.component';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  items = new Subject<object[]>();
  itemsForBasket: object[] = [];
  basket: object[] = [];
  basketOB = new Subject<object[]>();
  token;
  userId;
  basketLoadingOb = new Subject<boolean>();
  constructor(private http: HttpClient, private error: MatDialog, private router: Router) {

  }
  getItems(){
    this.basket = [];
    this.token = localStorage.getItem('token');
    this.userId = localStorage.getItem('userId');
    this.http.get('https://recipe-puppy.p.rapidapi.com/',{
      headers: new HttpHeaders({
        'x-rapidapi-key': '95b8c70fd8msh49a43938c25bf7ap1852cbjsnedf2ce8a16f5',
        'x-rapidapi-host': 'recipe-puppy.p.rapidapi.com',
        'useQueryString': 'true'
      })
    }).subscribe(data=>{
      //@ts-ignore
      this.items.next(data.results);
      //@ts-ignore
      this.itemsForBasket = data.results;
    }, error=>{
      console.log(error);
    })
    this.http.get('https://basket-a-default-rtdb.firebaseio.com/orders.json?auth='+this.token+ '&orderBy="userId"&equalTo="' + this.userId + '"').subscribe(data=>{
      for(var d in data){
        console.log('data: ',data[d]['orderData']);
        this.basket = data[d]['orderData'];
        this.basketOB.next(this.basket);
      }
    })
  }
  addBasket(index, price){
    let itemIndex:any = this.basket.findIndex(item=>{
      return item['index'] === index;
    });
    if(itemIndex === -1){
      this.basket.push(this.itemsForBasket[index]);
      this.basket[this.basket.length - 1]['price'] = price;
      this.basket[this.basket.length - 1]['index'] = index;
    }
    if(!this.basket[itemIndex===-1?this.basket.length-1:itemIndex]['count'])
    this.basket[this.basket.length-1]['count'] = 1;
    else{
      this.basket[itemIndex]['count'] = this.basket[itemIndex]['count'] + 1;
    }
    this.SaveDataToDatabase(false);
    return this.basket;
  }
  removeFromBasket(index){
    let itemIndex = this.basket.findIndex(item=>{
      return item['index'] === index;
    })
    if(this.basket[itemIndex]['count'] > 1){
      this.basket[itemIndex]['count'] = this.basket[itemIndex]['count'] - 1;
    }
    else{
      this.basket = this.basket.filter(item=>{
        return item['index'] !==index;
      })
      delete this.itemsForBasket[index]['count'];
    }
    if(this.basket.length === 0){
      this.SaveDataToDatabase(true);
    }
    else{
      this.SaveDataToDatabase(false);
    }
    return this.basket;
  }
  deleteFromBasket(index){
    this.basket = this.basket.filter(item=>{
      return item['index'] !==index;
    })
    delete this.itemsForBasket[index]['count'];
    if(this.basket.length === 0)
      this.SaveDataToDatabase(true);
    else
      this.SaveDataToDatabase(false);
    return this.basket;
  }
  SaveDataToDatabase(remove){
    this.token = localStorage.getItem('token');
    this.userId = localStorage.getItem('userId');
    var username;
    var SendData = {
      orderData: this.basket,
      userId: this.userId
    }
    //get
    this.http.get('https://basket-a-default-rtdb.firebaseio.com/orders.json?auth='+this.token+ '&orderBy="userId"&equalTo="' + this.userId + '"').subscribe(data=>{
      for(var d in data){
        username = d;
      }
      this.http.delete('https://basket-a-default-rtdb.firebaseio.com/orders/'+username+'.json?auth='+this.token).subscribe(data=>{

      },error=>{
        console.log(error);
      });
    },error=>{
      console.log(error);
    });
    //post
    if(!remove)
    this.http.post('https://basket-a-default-rtdb.firebaseio.com/orders.json?auth='+this.token,SendData).subscribe(data=>{
      this.basketLoadingOb.next(false);
    },error=>{
      console.log(error);
    });
    else{
      this.basketLoadingOb.next(false);
    }
  }
  CheckOut(){
    this.token = localStorage.getItem('token');
    this.userId = localStorage.getItem('userId');
    var username;
    this.http.get('https://basket-a-default-rtdb.firebaseio.com/orders.json?auth='+this.token+ '&orderBy="userId"&equalTo="' + this.userId + '"').subscribe(data=>{
      for(var d in data){
        username = d;
      }
      this.http.delete('https://basket-a-default-rtdb.firebaseio.com/orders/'+username+'.json?auth='+this.token).subscribe(data=>{

      },error=>{
        console.log(error);
      });
    },error=>{
      console.log(error);
    });
    this.basket = [];
    this.itemsForBasket.map(item=>{
      delete item['count'];
    })
    return this.basket;
  }

  signIn(email,password){
    const authData = {
      email: email,
      password: password,
      returnSecureToken: true
    }
    this.http.post('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyC9Yl63YTXIQbegf6CxYFpQICmLqKFUa9c',authData).subscribe(data=>{
      console.log(data);
      //@ts-ignore
      this.token = data.idToken;
      //@ts-ignore
      this.userId = data.localId;
      localStorage.setItem('token', this.token);
      localStorage.setItem('userId', this.userId);
      localStorage.setItem('email', email);
      this.router.navigate(['basket']);

    },error=>{
        this.error.open(PopUpErrorComponent,{data: {title: "Error", paragraph: error['error'].error.message}});
        setTimeout(()=>{
          this.error.closeAll();
        },1500);
    })
  }
  signUp(email,password){
    const authData = {
      email: email,
      password: password,
      returnSecureToken: true
    }
    this.http.post('https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyC9Yl63YTXIQbegf6CxYFpQICmLqKFUa9c',authData).subscribe(data=>{
      this.error.open(PopUpErrorComponent,{data: {title: "Success", paragraph: "Your account created successfully."}});
      setTimeout(()=>{
        this.error.closeAll();
      },1500);
    },error=>{
        this.error.open(PopUpErrorComponent,{data: {title: "Error", paragraph: error['error'].error.message}});
        setTimeout(()=>{
          this.error.closeAll();
        },1500);
    })
  }
  logOut(){
    localStorage.setItem('token','');
    localStorage.setItem('email', '');
    localStorage.setItem('userId', '');
    this.router.navigate(['signin']);
  }
}
