import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { PopUpErrorComponent } from '../pop-up-error/pop-up-error.component';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-sign-in-up',
  templateUrl: './sign-in-up.component.html',
  styleUrls: ['./sign-in-up.component.css']
})
export class SignInUpComponent implements OnInit {
  signUp = false;
  signForm: FormGroup;
  token;
  constructor(public error: MatDialog, private storage: StorageService, private router: Router) {
    this.token = localStorage.getItem('token');
    if(this.token){
      this.router.navigate(['basket']);
    }
  }

  ngOnInit(): void {
    this.signForm= new FormGroup({
      'email': new FormControl(null,[Validators.required, Validators.email]),
      'password': new FormControl(null, Validators.required)
    })
  }
  ChangeToggle(e){
    this.signUp = e.target.checked;
  }
  SignIn(){
    if(!this.signForm.valid){
      if(!this.signForm.get('password').valid && !this.signForm.get('email').valid) this.error.open(PopUpErrorComponent,{data: "Please, fill the fields"})
      else if(!this.signForm.get('email').valid) this.error.open(PopUpErrorComponent,{data: "Please enter valid email"})
      setTimeout(()=>{
        this.error.closeAll();
      },1600)
    }
    else{
      this.storage.signIn(this.signForm.value.email,this.signForm.value.password);
    }
  }
  SignUp(){
    if(!this.signForm.valid){
      if(!this.signForm.get('password').valid && !this.signForm.get('email').valid) this.error.open(PopUpErrorComponent,{data: "Please, fill the fields"})
      else if(!this.signForm.get('email').valid) this.error.open(PopUpErrorComponent,{data: "Please enter valid email"})
      setTimeout(()=>{
        this.error.closeAll();
      },1600)
    }
    else{
      this.storage.signUp(this.signForm.value.email,this.signForm.value.password);
    }
  }
}
