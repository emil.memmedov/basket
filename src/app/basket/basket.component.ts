import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DialogComponent } from '../dialog/dialog.component';
import { DoneDialogComponent } from '../done-dialog/done-dialog.component';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent {
  prices = [20,15,8,22,16,24,18,10,26,16];
  images = [
    'https://www.theseasonedmom.com/wp-content/uploads/2015/12/Ginger-Champagne-Cocktail-11A-500x500.jpg',
    'https://i1.wp.com/www.eatthis.com/wp-content/uploads/2019/01/healthy-twice-baked-potatoes.jpg?fit=1200%2C879&ssl=1',
    'https://embed.widencdn.net/img/mccormick/enasrrmqpx/1365x1365px/eggnog_thumbprint_cookies_6685.jpg',
    'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F11665.jpg&q=85',
    'https://media1.s-nbcnews.com/i/newscms/2018_11/1325222/irish-champ_-today-180316-tease-01_4157f01d87cac1fbaa791322e71ae788.jpg',
    'https://www.smalltownwoman.com/wp-content/uploads/2014/11/Chocolate-Cherry-Cookies-DSC_0067.jpg',
    'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F982753.jpg',
    'https://www.thespruceeats.com/thmb/QRwkO45Op0-1uXvm7FcOqSNwys8=/2880x1920/filters:fill(auto,1)/simple-hot-spiced-cider-3051519-hero-01-13c37f95fbdd4520a93a008f609f4b61.jpg',
    'https://www.chileanfoodandgarden.com/wp-content/uploads/2017/12/coffee-eggnog.jpg',
    'https://freezermealfrenzy.com/wp-content/uploads/2017/04/amysbbqpizza02.jpg'
  ]
  title = 'Basket';
  loading = true;
  items: object[] = [];
  basket = [];
  addToCartBtns = [];
  total = 0;
  basketCounts = [];
  email = "";
  basketLoading = false;
  token;
  constructor(private storage: StorageService, private dialog: MatDialog, private router: Router){
    this.token = localStorage.getItem('token');
    if(!this.token){
      this.router.navigate(['signin']);
    }
    this.email = localStorage.getItem('email');
    this.storage.basketLoadingOb.subscribe(state=>{
      this.basketLoading = state;
    })
    this.email = localStorage.getItem('email');
    this.storage.basketOB.subscribe(basket=>{
      this.basket = basket;
      for(let i = 0; i < this.basket.length; i++){
        if(this.basket[i]['count'] > 0){
          this.addToCartBtns[this.basket[i]['index']] = true;
          this.basketCounts[this.basket[i]['index']] = [this.basket[i]['count']];
        }
      }
    })
    this.storage.getItems();
    this.storage.items.subscribe(items=>{
      this.loading = false;
      this.items = items
      for(let i = 0; i < this.items.length;i++){
        this.addToCartBtns.push(false);
        this.basketCounts.push(0);
      }
    })
    this.CountTotal();
  }
  CountTotal(){
    this.total = 0;
    this.basket.map(item=>{
      this.total += (item.price*item.count)
    })
  }
  AddToCart(index,price){
    this.basketLoading = true;
    this.basket = this.storage.addBasket(index,price);
    this.addToCartBtns[index] = true;
    this.basketCounts[index]++;
    this.CountTotal();
  }
  removeFromCart(index){
    this.basketLoading = true;
    let item = this.basket.find(item=>{
      return item['index'] === index;
    })
    if(item['count'] === 1)
    this.addToCartBtns[index] = false;
    this.basket = this.storage.removeFromBasket(index);
    this.basketCounts[index]--;
    this.CountTotal();
  }
  deleteFromBasket(index){
    this.basketLoading = true;
    this.basket = this.storage.deleteFromBasket(index);
    this.addToCartBtns[index] = false;
    this.basketCounts[index] = 0;
    this.CountTotal();
  }
  CheckOut(){
    if(this.basket.length === 0){
      return;
    }
    let dialogRef = this.dialog.open(DialogComponent);
    dialogRef.afterClosed().subscribe(result=>{
      if(result === true){
        this.basket = this.storage.CheckOut();
        for(let i=0; i<this.items.length;i++){
          this.addToCartBtns[i] = false;
          this.basketCounts[i] = 0;
        }
        this.total = 0;
        this.dialog.open(DoneDialogComponent);
        setTimeout(()=>{
          this.dialog.closeAll();
        },2000)
      }
      else{
        return;
      }
    })
  }
  logOut(){
    this.storage.logOut();
  }
}
